from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    # path('join/', views.joinGroup, name='joinGroup'),
    path('form/', views.form, name='form'),
    path('profile/', views.profile, name='profile'),
    path('group/', views.group, name='group'),
    path('group/id/<str:uqStr>/', views.group_detail, name='group'),
    path('group/create/', views.create_group, name='create_group'),
    path('group/join/<str:uqStr>/', views.join_group, name='join_group'),
    path('group/left/<str:uqStr>/', views.left_group, name='left_group'),

]
