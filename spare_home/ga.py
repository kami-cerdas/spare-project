import calendar
import math
import random
import datetime
from numpy import cumsum
from numpy.random import rand

class Genetic_algorithm:

    def __init__(self, start, end, range_hour, span_hour, group_schedule):
        # format start, end 2019-05-01
        # format range hour 10:00-22:00
        # format span_hour hour:minute -> 01:30
        self.start = start 
        self.end = end
        self.range_hour = range_hour
        self.span_hour = span_hour

        # format group schedule dictionary, key = person id, value = list of the person schedule
        # inside the list is another list with [start, end]
        # schedule{id = [[start, end], [start, end]]}
        self.group_schedule = group_schedule

        fmt = '%Y-%m-%d'

        # format datetime.datetime(2019, 5, 7, 22, 30)
        start_datetime = datetime.datetime.strptime(self.start, fmt)
        end_datetime = datetime.datetime.strptime(self.end, fmt)
        
        # format datetime.timedelta(days)
        date_between = end_datetime - start_datetime
        span_days = date_between.days + 1
        self.days = span_days

        start_rangehour = self.range_hour[0:5]
        end_rangehour = self.range_hour[-5:]
        fmrt = '%H:%M'
        span_tuple = datetime.datetime.strptime(end_rangehour, fmrt) - datetime.datetime.strptime(start_rangehour, fmrt)
        span_minute = span_tuple.seconds / 60
        span_rangehour = int(span_minute / 60)
        self.span_rangehour = span_rangehour

        span_hour_datetime = datetime.datetime.strptime(self.span_hour, fmrt) - datetime.datetime.strptime("00:00", fmrt)
        span_hour_minute = span_hour_datetime.seconds / 60 
        span_hour_converted = span_hour_minute / 60
        self.span_hour_converted = span_hour_converted

    def fitness_function(self, individual):
        # format schedule dictionary, key = person id, value = list of the person schedule
        # inside the list is another list with [start, end]
        # schedule{id = [[start, end], [start, end]]}
        fitness_cost = 0  
        fmt = "%Y-%m-%d-%H:%M"
        # format individual individual = [start, end]
        for key in self.group_schedule:
            for i in range(len(self.group_schedule[key])):
                if ((datetime.datetime.strptime(self.group_schedule[key][i][0], fmt) < individual [0]) and
                    (datetime.datetime.strptime(self.group_schedule[key][i][1], fmt) < individual [0])):
                    continue

                if ((datetime.datetime.strptime(self.group_schedule[key][i][0], fmt) > individual [1]) and
                    (datetime.datetime.strptime(self.group_schedule[key][i][1], fmt) > individual [1])):
                    continue

                fitness_cost -= 1
                
        return fitness_cost

        
    def starting_population(self):
        # format range hour 10:00-22:00
        start_rangehour = self.range_hour[0:5]
        end_rangehour = self.range_hour[-5:]
        fmrt = '%H:%M'
        span_tuple = datetime.datetime.strptime(end_rangehour, fmrt) - datetime.datetime.strptime(start_rangehour, fmrt)
        span_minute = span_tuple.seconds / 60
        span_rangehour = int(span_minute / 60)

        total_starting_population = int(math.ceil((math.ceil((self.days * span_rangehour) / self.span_hour_converted)) * 0.75))

        first_generation = []

        for i in range(total_starting_population):
            new_individual = self.spawn_individual()
            first_generation.append(new_individual)

        return first_generation

    def spawn_individual(self):
        start_year = self.start[0:4]
        start_month = self.start[5:7]
        start_date = self.start[8:10]

        # iterate dates for random choosing
        # example range days = 6, date 1-7
        # iterate for i in 6, 1;2;3;4;5;6;7
        start = start_year + "/" + start_month + "/" + start_date
        start_datetime = datetime.datetime.strptime(start, "%Y/%m/%d")
        date_choices = []
        for i in range(self.days):
            inserted_date = start_datetime + datetime.timedelta(days = i)
            date_choices.append(inserted_date)

        # choose random date
        random_date = random.choice(date_choices)

        start_hour = self.range_hour[0:5]
        end_hour = self.range_hour[-5:]
        fmt = '%H:%M'
        start_time = datetime.datetime.strptime(start_hour, fmt)
        end_time = datetime.datetime.strptime(end_hour, fmt)
        # find the last possible hour to start the schedule
        end_start = end_time - datetime.timedelta(hours= int(self.span_hour[0:2]), minutes=int(self.span_hour[-2:]))

        # hour span between start and the last possible hour to start the schedule
        # format of span_start_end is datetime.timedelta(0, seconds)
        span_start_end = end_start - start_time 
        span_start_end_converted = int((span_start_end.seconds / 60) / 60)
        # variable for looping and find all available options for randoming the hour
        span_30minute = start_time
        hour_choice = []
        # append the first possible hour to start schedule
        hour_choice.append(start_time)
        # loop based on time span * 2 because of 30 minute loop
        for i in range((span_start_end_converted * 2)):
            span_30minute = span_30minute + datetime.timedelta(minutes=30)
            hour_choice.append(span_30minute)

        random_hour = random.choice(hour_choice)
        random_individual = (str(random_date.year) + "-" + str(random_date.month) + '-' + 
            str(random_date.day) + '-' + str(random_hour.hour) + ':' + str(random_hour.minute))
        fmt_datetime = "%Y-%m-%d-%H:%M"
        start_random_datetime = datetime.datetime.strptime(random_individual, fmt_datetime)
        end_random_datetime = start_random_datetime + datetime.timedelta(hours=int
            (self.span_hour[0:2]), minutes=int(self.span_hour[-2:]))
        
        individual_array = [start_random_datetime, end_random_datetime]

        return individual_array


    def mutate(self, individual):
        # mutation idea = + 1 hour and - 1 hour
        # another idea mutate the hour and the minute
        mutation_choice = ['date', 'hour']
        random_mutation = random.choice(mutation_choice)

        start_year = self.start[0:4]
        start_month = self.start[5:7]
        start_date = self.start[8:10]
        start = start_year + "/" + start_month + "/" + start_date
        start_datetime = datetime.datetime.strptime(start, "%Y/%m/%d")

        if random_mutation == 'date':
            date_choices = []
            for i in range(self.days):
                inserted_date = start_datetime + datetime.timedelta(days = i)
                date_choices.append(inserted_date)

            # choose random date
            random_date = random.choice(date_choices)
            individual[0] = datetime.datetime(year = random_date.year, month = random_date.month,
                day=random_date.day, hour=individual[0].hour, minute=individual[0].minute)
            individual[1] = datetime.datetime(year = random_date.year, month = random_date.month,
                day=random_date.day, hour=individual[1].hour, minute=individual[1].minute)
        elif random_mutation == 'hour':
            start_hour = self.range_hour[0:5]
            end_hour = self.range_hour[-5:]
            fmt = '%H:%M'
            start_time = datetime.datetime.strptime(start_hour, fmt)
            end_time = datetime.datetime.strptime(end_hour, fmt)
            # find the last possible hour to start the schedule
            end_start = end_time - datetime.timedelta(hours= int(self.span_hour[0:2]), minutes=int(self.span_hour[-2:]))
            span_start_end = end_start - start_time 
            span_start_end_converted = int((span_start_end.seconds / 60) / 60)
            # variable for looping and find all available options for randoming the hour
            span_30minute = start_time
            hour_choice = []
            hour_choice.append(start_time)
            # loop based on time span * 2 because of 30 minute loop
            for i in range((span_start_end_converted * 2)):
                span_30minute = span_30minute + datetime.timedelta(minutes=30)
                hour_choice.append(span_30minute)

            random_hour = random.choice(hour_choice)
            individual[0] = datetime.datetime(year = individual[0].year, month = individual[0].month,
                day=individual[0].day, hour=random_hour.hour, minute=random_hour.minute)
            individual[1] = individual[0] + datetime.timedelta(hours=int(self.span_hour[0:2]), 
                minutes=int(self.span_hour[-2:]))
        
        return individual

    def crossover(self, individual_1, individual_2):
        mutation_choice = ['date', 'hour']
        random_crossover = random.choice(mutation_choice)

        if random_crossover == 'date':
            temp = individual_1
            individual_1[0] = datetime.datetime(year= individual_2[0].year, month = individual_2[0].month,
                day=individual_2[0].day, hour=individual_1[0].hour, minute=individual_1[0].minute)
            individual_1[1] = datetime.datetime(year= individual_2[0].year, month = individual_2[0].month,
                day=individual_2[0].day, hour=individual_1[1].hour, minute=individual_1[1].minute)

            individual_2[0] = datetime.datetime(year= temp[0].year, month= temp[0].month,
                day=temp[0].day, hour=individual_2[0].hour, minute=individual_2[0].minute)
            individual_2[1] = datetime.datetime(year= temp[0].year, month = temp[0].month,
                day=temp[0].day, hour=individual_2[1].hour, minute=individual_2[1].minute)
        elif random_crossover == 'hour':
            temp = individual_1
            individual_1[0] = datetime.datetime(year= individual_1[0].year, month = individual_1[0].month,
                day=individual_1[0].day, hour=individual_2[0].hour, minute=individual_2[0].minute)
            individual_1[1] = datetime.datetime(year= individual_1[1].year, month = individual_1[1].month,
                day=individual_1[1].day, hour=individual_2[1].hour, minute=individual_2[1].minute)

            individual_2[0] = datetime.datetime(year= individual_2[0].year, month= individual_2[0].month,
                day=individual_2[0].day, hour=temp[0].hour, minute=temp[0].minute)
            individual_2[1] = datetime.datetime(year= individual_2[1].year, month= individual_2[1].month,
                day=individual_2[1].day, hour=temp[1].hour, minute=temp[1].minute)

        random_individual = [individual_1, individual_2]
        return random.choice(random_individual)

    def evolve_population(self, population):
        new_generation  = []
        mating_individual = self.select_mating_individual(population)

        for i in range(len(mating_individual)):
            new_generation.append(self.crossover(mating_individual[i], 
                mating_individual[random.randint(0, (len(mating_individual) - 1))]))

        for i in range(len(mating_individual)):
            new_generation[i] = self.mutate(new_generation[i])

        return new_generation

    def select_mating_individual(self, population):
        weights = []
        fitness_sum = 0
        for individual in population:
            fitness = self.fitness_function(individual)
            fitness_sum += fitness
            weights.append(fitness)

        for i in range(len(population)):
            weights[i] = weights[i] - fitness_sum

        mating = []
        for i in range(len(population)):
            mating_individual = population[random.randrange(0, len(population))]
            mating.append(mating_individual)

        return mating