from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import random, string, json
from account.models import Group
from .models import User
from account.models import CustomUser
# from .ga import all
# from django.contrib import messages


# Create your views here.
def index(request):
    return render(request, 'index.html')

def form(request):
    return render(request, 'form.html')

def profile(request):
    if request.user.is_authenticated:
        groups = request.user.group_set.all()
        response = {
            'groups': groups
        }
        return render(request, 'profile.html', response)
        
    return redirect('/')

def group(request):
    return render(request, 'group.html')

def group_detail(request, uqStr):
    group = Group.objects.filter(uniqueStr = uqStr)[0]
    response = {
        'group' : group,
        'members' : group.users.all(),
        'total_member' : len(group.users.all()),
        'joined' : request.user in group.users.all()
    }
    return render(request, 'group_detail.html', response)
    
def groupForm(request):
    return render(request, 'group_form.html')

def googleAPI(request):
    return render(request, 'googleAPI.html')


@csrf_exempt
def get_group_list(request):
    if request.user.is_authenticated:
        list_group = request.user.set_group.all()
        return HttpResponse(json.dumps({'message':list_group, 'status':'success'}), content_type="application/json")
    else:
        return redirect("/schedule")

@csrf_exempt
def create_group(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            if request.user:
                uniqueString = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
                group = Group.objects.create(
                    name = request.POST['name'],
                    description = request.POST['description'],
                    uniqueStr = uniqueString,
                    admin = request.user.name
                )
                group.users.add(request.user)

                return redirect('/group/id/' + uniqueString)
        else:
            return render(request, 'group_form.html')
    else:
        return redirect("/")

def view_group(request, uqStr):
    group = Group.objects.filter(uniqueStr = uqStr)
    response = {
        'group': group,
    }
    return render(request, 'group.html', response)

def left_group(request, uqStr):
    if request.user.is_authenticated:
        group = Group.objects.filter(uniqueStr = uqStr)[0]
        if request.user in group.users.all():
            group.users.remove(request.user)
            return redirect('/group/id/' + uqStr)

    else:
        return redirect("/schedule")
    

def join_group(request, uqStr):
    if request.user.is_authenticated:
        group = Group.objects.filter(uniqueStr = uqStr)[0]
        group.users.add(request.user)

        return redirect('/group/id/' + uqStr)

    else:
        return redirect("/schedule")                
