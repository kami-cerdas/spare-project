from django.apps import AppConfig


class SpareHomeConfig(AppConfig):
    name = 'spare_home'
