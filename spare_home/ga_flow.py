import ga

generation = 10
best_fitness = -999999999
start = "2019-05-04"
end = "2019-05-04"
range_hour = "08:00-15:00"
span_hour = "01:30"
best_schedule = ['0', '1']
group_schedule = {
    'Aryo':[
        ["2019-05-01-13:00", "2019-05-01-15:00"], 
        ["2019-05-03-08:00", "2019-05-03-09:40"], 
        ["2019-05-03-10:00", "2019-05-03-11:40"],
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"],
        ["2019-05-04-15:00", "2019-05-04-16:30"]],
    'Anit':[
        ["2019-05-02-14:00", "2019-05-02-15:00"], 
        ["2019-05-03-18:00", "2019-05-03-20:30"],
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"]],
    'Falya':[
        ["2019-05-03-10:00", "2019-05-03-15:00"], 
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"],
        ["2019-05-07-19:00", "2019-05-07-22:30"]
]}
best_dict = dict()

ga = ga.Genetic_algorithm(start, end, range_hour, span_hour, group_schedule)
population = ga.starting_population()
print("initial population")
print(population)
for individual in population:
    fitness = ga.fitness_function(individual)
    if fitness > best_fitness:
        best_fitness = fitness
        # best_schedule[0] = (str(individual[0].year) + '-' + str(individual[0].month) + '-' + str(individual[0].day) 
        #     + '-' + str(individual[0].hour) + ':' + str(individual[0].minute))
        # best_schedule[1] = (str(individual[1].year) + '-' + str(individual[1].month) + '-' + str(individual[1].day) 
        #     + '-' + str(individual[1].hour) + ':' + str(individual[1].minute))
        best_schedule[0] = str(individual[0].strftime("%d/%m/%Y, %H:%M"))
        best_schedule[1] = str(individual[1].strftime("%d/%m/%Y, %H:%M"))

for i in range(generation):
    population = ga.evolve_population(population)
    print("Generation: ", i+1)
    print(population)
    for individual in population:
        fitness = ga.fitness_function(individual)
        if fitness > best_fitness:
            best_fitness = fitness
            # best_schedule[0] = (str(individual[0].year) + '-' + str(individual[0].month) + '-' + str(individual[0].day) 
            # + '-' + str(individual[0].hour) + ':' + str(individual[0].minute))
            # best_schedule[1] = (str(individual[1].year) + '-' + str(individual[1].month) + '-' + str(individual[1].day) 
            #     + '-' + str(individual[1].hour) + ':' + str(individual[1].minute))

            best_schedule[0] = str(individual[0].strftime("%d/%m/%Y, %H:%M"))
            best_schedule[1] = str(individual[1].strftime("%d/%m/%Y, %H:%M"))


print("Best Schedule: ")
print(best_schedule)
# best schedule output ['04/05/2019, 12:00', '04/05/2019, 13:30']
print("Best Fitness: ")
print(best_fitness)
