import ga

generation = 10
best_fitness = -999999999
start = "2019-05-04"
end = "2019-05-04"
range_hour = "08:00-13:00"
span_hour = "01:30"
best_schedule = ""
group_schedule = {
    'Aryo':[
        # ["2019-05-01-13:00", "2019-05-01-15:00"], 
        # ["2019-05-03-08:00", "2019-05-03-09:40"], 
        # ["2019-05-03-10:00", "2019-05-03-11:40"],
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"],
        ["2019-05-04-15:00", "2019-05-04-16:30"]],
    'Anit':[
        # ["2019-05-02-14:00", "2019-05-02-15:00"], 
        # ["2019-05-03-18:00", "2019-05-03-20:30"],
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"]],
    'Falya':[
        # ["2019-05-03-10:00", "2019-05-03-15:00"], 
        ["2019-05-04-08:00", "2019-05-04-09:40"], 
        ["2019-05-04-10:00", "2019-05-04-11:40"],
        # ["2019-05-07-19:00", "2019-05-07-22:30"],
]}
best_dict = dict()

ga = ga.Genetic_algorithm(start, end, range_hour, span_hour, group_schedule)
population = ga.starting_population()
print("initial population")
print(population)
for individual in population:
    fitness = ga.fitness_function(individual)
    if fitness > best_fitness:
        best_fitness = fitness
        best_schedule = str(individual)
        print('-------------------best schedule initial----------------------')
        print(best_schedule)
        print('------------------- fitness -------------------')
        print(best_fitness)
        if str(best_fitness) in best_dict:
            pass
        else:
            best_dict.update({str(best_fitness): best_schedule})
        print('-------------------best dict initial----------------------')
        print(best_dict)     
    # print('local')
    # print(locals())

print('-------------- best schedule diluar for --------------------')
print(best_schedule)
print(id(best_schedule))
print('-------------- best fitness diluar for --------------------')
print(best_fitness)
print('-------------------best dict diluar for----------------------')
print(best_dict)   

for i in range(generation):
    print('-------------- best schedule sebelum evolve --------------------')
    print(best_schedule)
    print(id(best_schedule))
    print('-------------- best fitness sebelum evolve --------------------')
    print(best_fitness)
    print('-------------------best dict sebelum evolve----------------------')
    print(best_dict) 
    population = ga.evolve_population(population)
    print("Generation: ", i+1)
    print(population)
    print('-------------------best schedule----------------------')
    print(best_schedule)
    print(id(best_schedule))
    print('------------------- fitness -------------------')
    print(best_fitness)
    print('-------------------best dict setelah evolve sebelum for----------------------')
    print(best_dict) 
    for individual in population:
        fitness = ga.fitness_function(individual)
        if fitness > best_fitness:
            print('MASUK IF IF IF IF IF IF')
            best_fitness = fitness
            best_schedule = individual
            print('------------------best schedule dalam if -----------------------')
            print(best_schedule)
            print('-----------------best fitness dalam if----------------')
            print(best_fitness)
            if str(best_fitness) in best_dict:
                pass
            else:
                best_dict.update({str(best_fitness): best_schedule})

            print('-------------------best dict dalam if----------------------')
            print(best_dict) 

print('global:')
print(globals())
print("Best Schedule: ")
print(best_schedule)
print("Best Fitness: ")
print(best_fitness)
print('best dict')
print(best_dict)
