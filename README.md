# SPARE AI PROJECT

Sistem berbasis AI yang akan memudahkan pengguna untuk menggabungkan jadwal dan mencari waktu kosong dari jadwal yang diinput pengguna

```
spare.herokuapp.com
```

## Meet Our Team

* **Falya Aqiela Sekardina** 
* **Aryo Tinulardhi**
* **Janitra Ariena Sekarputri**
* **Millenio Ramadizsa**    

### Prerequisites

1. Clone Repo ini

```
git clone https://gitlab.com/kami-cerdas/spare-project.git
```

2. Buat branch baru

```
git checkout -b 'nama branch'
```

3. Bikin app baru di branch itu

```
python3 manage.py startapp 'nama-app'
```

4. push nya ke branch masing-masing, kalo udah selesai semua tinggal merge request ke master