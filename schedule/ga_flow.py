from . import ga as ga_algo

def find_schedule(generation, start, end, range_hour, span_hour, group_schedule):
    best_schedule = [0, 0]
    best_fitness = -999999999
    ga = ga_algo.Genetic_algorithm(start, end, range_hour, span_hour, group_schedule)
    population = ga.starting_population()
    for individual in population:
        fitness = ga.fitness_function(individual)
        if fitness > best_fitness:
            best_fitness = fitness
            best_schedule[0] = str(individual[0].strftime("%d/%m/%Y, %H:%M"))
            best_schedule[1] = str(individual[1].strftime("%d/%m/%Y, %H:%M"))

    for i in range(generation):
        population = ga.evolve_population(population)
        for individual in population:
            fitness = ga.fitness_function(individual)
            if fitness > best_fitness:
                best_fitness = fitness

                best_schedule[0] = str(individual[0].strftime("%d/%m/%Y, %H:%M"))
                best_schedule[1] = str(individual[1].strftime("%d/%m/%Y, %H:%M"))

    return (best_schedule, best_fitness)
