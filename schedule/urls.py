from django.urls import path
from . import views

urlpatterns = [
    path('', views.googleAPI, name='googleAPI'),
    path('/login/auth/', views.google_auth, name='google_auth'),
    path('/find/', views.get_group_schedules, name="get_group_schedules"),
    path('/form/<str:uqStr>/', views.get_group_form, name="get_group_form"),
]
