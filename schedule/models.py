from django.db import models
from account.models import CustomUser

# Create your models here.
class Schedule(models.Model):
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE, default=0)
    date_time = models.CharField(max_length=40, default="")
    queue = models.IntegerField()

    def __str__(self):
        return str(self.queue) + " - " + self.owner.name