from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from spare_home.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json
from account.models import Group
from . import ga_flow
from schedule.models import Schedule
# import datetime


# Create your views here.

def googleAPI(request):
    return render(request, 'googleAPI.html')

@csrf_exempt
def google_auth(request):
    if request.method == "POST":
        user = User.objects.filter(email=request)
        if not user:
            # user = User.objects.create(
            #     name = request.name,
            #     email = request.email,
            #     profpic = request.profpic
            # )
            print('create object!')
        request.email = request.POST['email']
        return HttpResponse(json.dumps({'message':'Logged in!', 'status':'success'}), content_type="application/json")
        # JsonResponse({
        #     'message':'Logged in!',
        #     'status':'success'
        # })
    else :
        return redirect('/')


def get_group_form(request, uqStr):
    if request.user:
        group = Group.objects.filter(uniqueStr = uqStr)[0]
        users = group.users.all()

        if request.user in users:
            return render(request, 'form.html', {'group':group})
            
    return redirect('/group/id/' + uqStr)
    
@csrf_exempt
def get_group_schedules(request):
    if request.method == "POST":
        uqStr = request.POST['group']
        group = Group.objects.filter(uniqueStr = uqStr)[0]
        users = group.users.all()

        if request.user in users:

            if request.user:
                schedules = {}
                if request.user in users:
                    for user in users:
                        schedules[user.email] = [[i for i in date_time['date_time'].split('_')] for date_time in user.schedule_set.order_by('queue').values('date_time')]

                generation = 10
                start = request.POST['start_date']
                end = request.POST['end_date']
                range_hour = request.POST['start_hour'] + ":" + request.POST['start_minute'] + "-" + request.POST['end_hour'] + ":" + request.POST['end_minute']
                span_hour = request.POST['range_hour'] + ":" + request.POST['range_minute']

                result = ga_flow.find_schedule(
                    generation=generation, 
                    start = start, 
                    end = end, 
                    range_hour = range_hour, 
                    span_hour = span_hour, 
                    group_schedule=schedules
                )
                res_start = result[0][0].split(', ')
                res_end = result[0][1].split(', ')

                group.lastSpareDate = res_start[0]
                group.lastSpareTime = res_start[1] + " - " + res_end[1]
                group.lastSpareFitness = str(result[1])
                group.save()

                return redirect('/group/id/' + uqStr)



    return redirect('/')
 





