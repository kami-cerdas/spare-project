// Client ID and API key from the Developer Console
var CLIENT_ID = '43921813824-mt5en8ksc3ml6vjhm94mupd7ioei6t7v.apps.googleusercontent.com';
var API_KEY = 'AIzaSyDswUezwLcNEzj4BaLwswbiU5vt8Ty2RSg';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
// var SCOPES = "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.readonly";
var SCOPES = "https://www.googleapis.com/auth/calendar";

var authorizeButton = document.getElementById('authorize_button');
var signoutButton = document.getElementById('signout_button');

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    apiKey: API_KEY,
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  }, function (error) {
    appendPre(JSON.stringify(error, null, 2));
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    titleAuth.style.display = 'none';
    titleSignin.style.display = 'block';
    loginDjango();
    writeEvents();
    listUpcomingEvents();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    titleAuth.style.display = 'block';
    titleSignin.style.display = 'none';
    // document.getElementById('sum-card').remove(document.getElementById('appendSum'));
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  $.ajax({
    url: "account/logout/",
    type : 'GET',
    success: function(data){
        console.log(data);
    }
  })
  gapi.auth2.getAuthInstance().signOut();
}

// function writeEvents() {
//   var resource = {
//     'summary': 'Google I/O 2015',
//     'location': '800 Howard St., San Francisco, CA 94103',
//     'description': 'A chance to hear more about Google\'s developer products.',

//     'start': {
//       'dateTime': '2019-11-28T09:00:00+07:00',
//       'timeZone': 'America/Los_Angeles',
//     },
//     'end': {
//       'dateTime': '2019-11-28T17:00:00+07:00',
//       'timeZone': 'America/Los_Angeles',
//     },
//   };
//   gapi.client.load('calendar', 'v3', function () {
//     var request = gapi.client.calendar.events.insert({
//       'calendarId': 'primary',
//       'resource': resource,
//       // 'eventId' : 'ODIwMTFkaHZudjh0OTUxdTRxZWJqN3A5bjhfMjAxOTExMjhUMDIwMDAwWiBmYWx5YXNla2FyZGluYUBt'
//     });
//     request.execute(function (response) {
//       console.log(response);
//     });
//   });
// };

function loginDjango() {
  var general = gapi.auth2.getAuthInstance().currentUser.get();
  var profile = general.getBasicProfile();
  console.log(profile);
  var data = {
    'email' : profile.U3,
    'name' : profile.ig,
    'profpic' : profile.Paa,
    'password' : general.El,
  }
  console.log(data);
  $.ajax({
    url: "account/login/",
    type : 'POST',
    data : data,
    datatype: 'json',
    success: function(data){
        console.log(data);
    }
  })
}


