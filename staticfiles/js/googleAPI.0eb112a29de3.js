{/* <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()"
    onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script> */}
// Client ID and API key from the Developer Console
var CLIENT_ID = '43921813824-mt5en8ksc3ml6vjhm94mupd7ioei6t7v.apps.googleusercontent.com';
var API_KEY = 'AIzaSyDswUezwLcNEzj4BaLwswbiU5vt8Ty2RSg';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

var authorizeButton = document.getElementById('authorize_button');
var signoutButton = document.getElementById('signout_button');

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
    gapi.client.init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
    }).then(function () {
        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

        // Handle the initial sign-in state.
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        authorizeButton.onclick = handleAuthClick;
        signoutButton.onclick = handleSignoutClick;
    }, function (error) {
        appendPre(JSON.stringify(error, null, 2));
    });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        authorizeButton.style.display = 'none';
        signoutButton.style.display = 'block';
        listUpcomingEvents();
    } else {
        authorizeButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendHeader(message) {
    var header = document.getElementById('header');
    var textContent = document.createTextNode(message + '\n');
    header.appendChild(textContent);
}

function appendContent(message) {
    var content = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    content.appendChild(textContent);
}

function appendSumCard(message1, message2, label) {
    var content = document.getElementById('sum-card');
    // var textContent1 = document.createTextNode(message1 + '\n');
    // var textContent2 = document.createTextNode(message2 + '\n');
    var res =
        '<label class="label-custom">' + label + '</label>' +
        '<div class="card custom-content">' +
        '<div class="card-header" id="header">' +
        message1 +
        '</div>' +
        '<ul class="list-group list-group-flush">' +
        '<li class="list-group-item" id="content"> ' +
        message2 +
        '</li>' +
        '</ul>' +
        '</div>';
    content.innerHTML += res;
    console.log('done')
}

/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {
    gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 10,
        'orderBy': 'startTime'
    }).then(function (response) {
        var events = response.result.items;

        if (events.length > 0) {
            for (i = 0; i < events.length; i++) {
                var event = events[i];
                var whenStart = event.start.dateTime;
                var whenEnd = event.end.dateTime;
                var splittedStart = whenStart.split("T");
                var whenStartDate = splittedStart[0];
                var whenStartTime = splittedStart[1];
                var splittedEnd = whenEnd.split("T");
                var whenEndDate = splittedEnd[0];
                var whenEndTime = splittedEnd[1];
                // appendHeader(event.summary)
                // appendContent(whenStart + ' until ' + whenEnd)
                appendSumCard(event.summary, whenStartTime + ' until ' + whenEndTime, whenStartDate + ' - ' + whenEndDate);
            }
        } else {
            appendHeader('No upcoming events found.');
        }
    });
}
