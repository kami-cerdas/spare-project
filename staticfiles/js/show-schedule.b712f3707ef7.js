/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendHeader(message) {
    var header = document.getElementById('header');
    var textContent = document.createTextNode(message + '\n');
    header.appendChild(textContent);
  }
  
  function appendContent(message) {
    var content = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    content.appendChild(textContent);
  }
  
  function appendSumCard(message1, message2, label) {
    var content = document.getElementById('sum-card');
    // var textContent1 = document.createTextNode(message1 + '\n');
    // var textContent2 = document.createTextNode(message2 + '\n');
    var res =
      '<div class="card custom-content" id="appendSum">' +
      '<label class="label-custom">' + label + '</label>' +
      '<div class="card card-wrap">' +
      '<div class="card-header" id="header">' +
      message1 +
      '</div>' +
      '<ul class="list-group list-group-flush">' +
      '<li class="list-group-item" id="content"> ' +
      message2 +
      '</li>' +
      '</ul>' +
      '</div>' +
      '</div>'
    '</div>';
    content.innerHTML += res;
  }