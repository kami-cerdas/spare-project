/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {
    gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 20,
        'orderBy': 'startTime'
    }).then(function (response) {
        var events = response.result.items;
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var result;
        if (events.length > 0) {
            for (i = 0; i < events.length; i++) {
                var event = events[i];
                var whenStart = event.start.dateTime;
                var whenEnd = event.end.dateTime;
                var splittedStart = whenStart.split("T");
                var whenStartDateMonth = Number((splittedStart[0].split('-'))[1]);
                var whenStartDate = (splittedStart[0].split('-'))[2];
                var whenStartDateYear = (splittedStart[0].split('-'))[0]
                //   var whenStartTimeGMT = Number(((((splittedStart[1]).split("+"))[0]).split(':'))[0]) + 7;
                var whenStartTime = ((((splittedStart[1]).split("+"))[0]).split(':'))[0] + ':' + ((((splittedStart[1]).split("+"))[0]).split(':'))[1]
                var splittedEnd = whenEnd.split("T");
                var whenEndDateMonth = Number((splittedEnd[0].split('-'))[1]);
                var whenEndDate = (splittedEnd[0].split('-'))[2];
                var whenEndDateYear = (splittedEnd[0].split('-'))[0]
                //   var whenEndTimeGMT = Number(((((splittedEnd[1]).split("+"))[0]).split(':'))[0]) + 7;
                var whenEndTime = ((((splittedEnd[1]).split("+"))[0]).split(':'))[0] + ':' + ((((splittedEnd[1]).split("+"))[0]).split(':'))[1]
                //   appendSumCard(event.summary, whenStartTime + ' until ' + whenEndTime, whenStartDate + ' ' + months[whenStartDateMonth - 1] + ' ' + whenStartDateYear + ' - ' + whenEndDate + ' ' + months[whenEndDateMonth - 1] + ' ' + whenEndDateYear);
                result.push(whenStartDate + "-" + whenStartDateMonth + "-" + whenStartDateYear + "_" + whenStartTime + "_" + whenEndDate + "-" + whenEndDateMonth + "-" + whenEndDateYear + "_" + whenEndTime);
            }
        }
        console.log(result);
        return result;
    });
}