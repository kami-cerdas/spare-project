from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=50)
    email = models.EmailField(_('email address'), unique=True)
    profpic = models.CharField(max_length=1000)
    
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Group(models.Model):
    name = models.CharField(max_length=30)
    users = models.ManyToManyField(CustomUser)
    admin = models.CharField(max_length=50, default="")
    uniqueStr = models.CharField(max_length=20)
    description = models.CharField(max_length=200, default="")
    lastSpareDate = models.CharField(max_length=30, default="-")
    lastSpareTime = models.CharField(max_length=15, default="-")
    lastSpareFitness = models.CharField(max_length=4, default="-")
    

    def __str__(self):
        return self.name

