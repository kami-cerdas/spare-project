from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='logout'),
    path('signup/', views.register, name='register'),
    path('schedule/store/', views.store, name='store'),
]
