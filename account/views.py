from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from .forms import UserForm
import json
from django.views.decorators.csrf import csrf_exempt
from .models import Group
from schedule.models import Schedule

# Create your views here.
@csrf_exempt
def user_login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(email=email, password=password)
        if user:
            login(request, user)
            return HttpResponse(json.dumps({'message':'Logged in!', 'status':'success'}), content_type="application/json")

        else:
            return register(request)
    else:
        return redirect('/')

def user_logout(request):
    logout(request)
    return HttpResponse(json.dumps({'message':'Logged out!', 'status':'success'}), content_type="application/json")


@csrf_exempt
def register(request):
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid() :
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            return HttpResponse(json.dumps({'message':'Success!', 'status':'success'}), content_type="application/json")

        else:
            return HttpResponse(json.dumps({'message':'Failed!', 'status':'fail'}), content_type="application/json")
    return HttpResponse()

@csrf_exempt
def store(request):
    if request.method == 'POST':

        schedules_exist = Schedule.objects.filter(owner=request.user).delete()

        schedules = request.POST.getlist('schedules[]')

        for queue, item in enumerate(schedules):
            schedule = Schedule.objects.create(
                owner = request.user,
                date_time = item,
                queue=queue,
            )
            request.user.schedule_set.add(schedule)
        return HttpResponse(json.dumps({'message':'Data added!', 'status':'success'}), content_type="application/json")





