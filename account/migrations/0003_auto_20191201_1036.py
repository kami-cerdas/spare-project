# Generated by Django 2.1.5 on 2019-12-01 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20191130_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='lastSpareFitness',
            field=models.CharField(default='', max_length=4),
        ),
    ]
